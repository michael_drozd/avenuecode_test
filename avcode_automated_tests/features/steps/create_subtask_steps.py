from nose.tools import assert_equal, assert_true
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions as EC
from datetime import date
import datetime
import time


@given(u'a web browser is on MyTasks page')
def step(context):
	context.browser.get("https://qa-test.avenuecode.com/tasks")
	assert_equal(context.browser.title, "ToDo Rails and Angular")

@when(u'the user is adding subtasks')
def step(context):
	context.browser.find_element(By.XPATH, "//button[.='(0) Manage Subtasks']").click()
	new_subtask = context.browser.find_element(By.ID, "new_sub_task")
	new_subtask.send_keys('Subtask to an existing task')
	
	# Setting a day for subtask
	set_day = context.browser.find_element_by_id("dueDate")
	set_day.clear()
	now = datetime.datetime.now()
	date = now.strftime("%m/%d/%Y")
	set_day.send_keys(date)
	time.sleep(2)
	add_subtask = context.browser.find_element_by_id("add-subtask").click()

@then(u'the subtask should be added to the list')
def step(context):
	sub_task = context.browser.find_element(By.XPATH, "//tr[@ng-repeat = 'sub_task in task.sub_tasks']")
	assert sub_task.is_displayed()

