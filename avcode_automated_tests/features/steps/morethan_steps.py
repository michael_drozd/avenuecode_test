from nose.tools import assert_equal, assert_true
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
import time
import csv

@given(u'user on main page is already authorized')
def step(context):
	context.browser.get("https://qa-test.avenuecode.com/tasks")
	assert_equal(context.browser.title, "ToDo Rails and Angular")

@when(u'we are adding a task with more than 250 characters')
def step(context):
	with open('./features/steps/long_text.txt', 'r') as f:
		lines = f.read().strip()
		context.browser.find_element(By.ID, "new_task").send_keys(lines)
		context.browser.find_element(By.XPATH, "//span[@ng-click = 'addTask()']").click()
		time.sleep(2)

@then(u'error message should appears')
def step(context):
	added_task = context.browser.find_element(By.XPATH, "//*[contains(text(), 'Far far away')]")
	assert added_task.is_displayed()


