from nose.tools import assert_equal, assert_true
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
import time


@given(u'user on MyTasks page')
def step(context):
	context.browser.get("https://qa-test.avenuecode.com/tasks")
	assert_equal(context.browser.title, "ToDo Rails and Angular")

@when(u'the user is adding subtasks with empty description and due date')
def step(context):
	context.browser.find_element(By.XPATH, "//button[.='(0) Manage Subtasks']").click()
	new_subtask = context.browser.find_element(By.ID, "new_sub_task")
	new_subtask.clear()
	
	# Setting a day for subtask
	set_day = context.browser.find_element_by_id("dueDate")
	set_day.clear()
	add_subtask = context.browser.find_element_by_id("add-subtask").click()

@then(u'error appears, subtasks description and due date are required fields')
def step(context):
	sub_task = context.browser.find_element(By.XPATH, "//tr[@ng-repeat = 'sub_task in task.sub_tasks']")
	assert sub_task.is_displayed()

