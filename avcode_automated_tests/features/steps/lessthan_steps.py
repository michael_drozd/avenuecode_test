from nose.tools import assert_equal, assert_true
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
import time

@given('user on main page is signing in')
def step(context):
	context.browser.get("https://qa-test.avenuecode.com/")
	assert_equal(context.browser.title, "ToDo Rails and Angular")


@when('I am opening MyTasks')
def step(context):
	context.browser.find_element(By.XPATH, "//a[@role='button']").click()
	context.browser.implicitly_wait(10)

@then('we are adding task name with 2 characters')
def step(context):
	task_name = 'AA'
	context.browser.find_element(By.ID, "new_task").send_keys(task_name)
	context.browser.find_element(By.XPATH, "//span[@ng-click = 'addTask()']").click()
	time.sleep(2)
	added_task = context.browser.find_element(By.XPATH, "//*[contains(text(), 'AA')]")
	assert added_task.is_displayed()


