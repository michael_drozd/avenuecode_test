from nose.tools import assert_equal, assert_true
from selenium.webdriver.common.by import By

@given(u'a user visits the site')
def impl(context):
	context.browser.get('https://qa-test.avenuecode.com/')

@when(u'I log in as "registeredUser"')
def step_impl(context):
	sign_in = context.browser.find_element(By.XPATH, "//a[@href = '/users/sign_in']").click()
	username_field = context.browser.find_element(By.ID, "user_email")
	password_field = context.browser.find_element(By.ID, "user_password")
	username_field.send_keys('mykhailo.drozd@gmail.com')
	password_field.send_keys('QWErty654321')
	submit_button = context.browser.find_element(By.XPATH, "//input[@name='commit']")
	submit_button.click()

@then(u'I should see the message {auth_message}')
def imple(context, auth_message):
	message = context.browser.find_element(By.LINK_TEXT, "Welcome, Mykhailo Drozd!")
	assert message.text == auth_message


"""
@given('I navigate to the ToDo App Home page')
def step(context):
	context.browser.get("https://qa-test.avenuecode.com/")
	context.browser.implicitly_wait(10)
	assert_equal(context.browser.title, "ToDo Rails and Angular")

#@when('I am clicking on Sign In')
#def step(context):
#	context.browser.find_element(By.XPATH, "//a[@href = '/users/sign_in']").click()

#@then('I am seeing the Sign in form')
#def step(context):
#	sign_in = context.browser.find_element(By.XPATH, "//h4[.='Sign in']")
#	sign_in.is_displayed()

#@then('I signing in to account')
#def step(context):
#	email = context.browser.find_element(By.ID, "user_email").send_keys('mykhailo.drozd@gmail.com')
#	password = context.browser.find_element(By.ID, "user_password").send_keys('QWErty654321')

@then('I am Clicking MyTasks')
def step(context):
	context.browser.find_element(By.XPATH, "//a[@role='button']").click()
	context.browser.implicitly_wait(10)

@then('User seeing the title Hey Mykhailo, this is your todo list for today')
def step(context):
	assert_equal(context.browser.title, "ToDo Rails and Angular")
"""

