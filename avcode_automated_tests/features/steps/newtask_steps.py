from nose.tools import assert_equal, assert_true
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import time

@given('opening MyTasks page')
def step(context):
	context.browser.get("https://qa-test.avenuecode.com/tasks")
	context.browser.implicitly_wait(10)

@then('adding a valid task name')
def step(context):
	task_name = context.browser.find_element(By.ID, "new_task")
	task_name.send_keys('New task')

@then('clicking add button')
def step(context):
	context.browser.find_element(By.XPATH, "//span[@ng-click = 'addTask()']").click()

@then('adding another task name and submitting it by Enter key')
def step(context):
	task_name = 'New task %s' % time.time()

	context.browser.find_element(By.ID, "new_task").send_keys(task_name)

	active_web_element = context.browser.switch_to_active_element()
	active_web_element.send_keys(Keys.ENTER)



