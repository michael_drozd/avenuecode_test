from nose.tools import assert_equal, assert_true
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC


@given(u'a user visits main page of site')
def impl(context):
	context.browser.get('https://qa-test.avenuecode.com/')
	context.browser.implicitly_wait(10)

@when(u'user clicks on MyTasks')
def step(context):
	mytasks = context.browser.find_element(By.XPATH, "//a[@role='button']")
	mytasks.is_displayed()
	mytasks.click()


@then(u'user should see the message {welcome_message}')
def step(context, welcome_message):
	visible_message = context.browser.find_element(By.XPATH, "//h1")
	print (visible_message.text)
	assert visible_message.text == welcome_message
