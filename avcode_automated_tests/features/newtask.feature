Feature: Newtask

  Scenario: Adding a new task to the list
    Given opening MyTasks page
    Then adding a valid task name
    Then clicking add button
    Then adding another task name and submitting it by Enter key
