Feature: SubTask Description (250 characters) and SubTask due date (MM/dd/yyyy format)

  Scenario: Create SubTask with long description and valid date format
    Given the MyTasks page is opened
    When the user is adding subtasks with 250 characters
    Then the subtask with 250 characters successfully added to the list