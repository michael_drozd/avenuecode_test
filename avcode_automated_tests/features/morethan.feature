Feature: The task can’t have more than 250 characters

  Scenario: Create a task with a name more than 250 characters
    Given user on main page is already authorized
    When we are adding a task with more than 250 characters
    Then error message should appears