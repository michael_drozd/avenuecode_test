Feature: MyTasks welcome screen

  Scenario Outline: The user should always see the My Tasks link on the NavBar
    Given a user visits main page of site
    When user clicks on MyTasks
    Then user should see the message <welcome message>

  Examples: MyTasks
	| welcome message               |
    | Hey Mykhailo, this is your todo list for today: |