Feature: Subtask description and due date are required fields

  Scenario: Create SubTask with empty description and due date
    Given user on MyTasks page
    When the user is adding subtasks with empty description and due date
    Then error appears, subtasks description and due date are required fields