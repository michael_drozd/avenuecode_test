Main page is: https://qa-test.avenuecode.com/

Email: mykhailo.drozd@gmail.com
Password: QWErty654321

Test stack:
- Python 3.x
- Selenium WebDriver
- Behave (Manual for Behave - http://behave.readthedocs.io/en/latest/index.html#)
- Firefox

Setup:
pip install selenium
pip install behave
pip install nose


To run test from the console:
 - From Terminal open "avcode_automated_tests" directory
 - Insert 'behave' command and hit Enter key.